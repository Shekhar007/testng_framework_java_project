package Test_Package;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.Test;

import API_Common_Methods.java.PUT_Common_Method;
import Endpoint.PUT_Endpoint;
import Request_Repository.PUT_Request_Repository;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import io.restassured.path.json.JsonPath;

public class PUT_TestCase_1 extends PUT_Common_Method{
	@Test
	public static void executor ()throws IOException 
	{
		File log_dir = Handle_directory.create_log_directory("PUT_TestCase_1_logs");

		String requestBody =PUT_Request_Repository.PUT_Request_Repository_Tc1();
		String endpoint =PUT_Endpoint.PUT_Endpoint_Tc1();
		for(int i=0; i<5; i++)
		{
			int statusCode = put_statusCode(requestBody ,endpoint);
			System.out.println("PUT API Triggered");
			System.out.println(statusCode);
				
			if(statusCode == 200)
			{
				String responseBody =PUT_responseBody(requestBody,endpoint);
				System.out.println(responseBody);
				
				Handle_api_logs.evidence_creator(log_dir, "PUT_TestCase", endpoint, requestBody, responseBody);
                PUT_TestCase_1.validator(requestBody, responseBody);
				break;
			}
			else
			{
				System.out.println("Expected statuscode of PUT API (200) is not found , hence retrying");			}
		}

	}


	private static String PUT_responseBody(String requestBody, String endpoint) {
		// TODO Auto-generated method stub
		return requestBody;
	}


	public static void validator(String requestBody, String responseBody)
	{
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		String req_createdAt = jsp_req.getString("createdAt");
		LocalDateTime currentdate = LocalDateTime.now();

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdAt = jsp_res.getString("createdAt");

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdAt, req_createdAt);
	}
}
