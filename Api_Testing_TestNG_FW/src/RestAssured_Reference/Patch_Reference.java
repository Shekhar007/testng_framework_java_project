package RestAssured_Reference;

import io.restassured.RestAssured;

import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;
import java.time.LocalDateTime;
import org.testng.Assert;

public class Patch_Reference {
	public static void main(String[] args) {
//Step1 : Declare Base URL;
		RestAssured.baseURI = "https://reqres.in/";
//Step2 : Configure request parameters and trigger the API
		String requestbody = ("{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}");
		String responsebody = given().header("Content-Type", "application/json").body(requestbody).when()
				.put("/api/user/2").then().log().all().extract().response().asString();
		System.out.println("responsebody is :" + responsebody);
//step3 : create an object of json path to parse requestbody and then the responsebody 
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		String req_createdAt = jsp_req.getString("createdAt");
		LocalDateTime currentdate = LocalDateTime.now();
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdAt = jsp_res.getString("createdAt");
//step4 : Validate responsebody
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdAt, req_createdAt);
	}

}